# Getting Started

### Database Setup
To run MongoDB locally:

`docker run -d -p 27017:27017 --name some-mongo -e MONGO_INITDB_DATABASE=news -e MONGO_INITDB_ROOT_USERNAME=user -e MONGO_INITDB_ROOT_PASSWORD=password mongo:latest`

or

`docker-compose -f docker-compose.yml up`


- MongoDB через модуль Spring Data Mongo Reactive обеспечивает полностью реактивные и неблокирующие взаимодействия с базой данных NoSQL, а также надежное управление обратным давлением;

- Replace Spring Web MVC module with Spring WebFlux to use non-blocking server (Netty instead of Tomcat).
  WebFlux действует намного эффективнее в терминах пропускной способности, задержки и потребления процессорного времени.
  Web MVC - неэффективная модель: 1 поток для каждого соединения.

- WebSocket - two-side asynchronous communication between client and server - is out of scope.

- UI for web application on server side (thymeleaf template/ JSP/ FreeMarker)
  По умолчанию WebFlux поддерживает только механизм FreeMarker.

- Thymeleaf - асинхронное и потоковое отображения шаблонов. начинает потоковую передачу данных клиенту, не дожидаясь получения последнего элемента.

### Подписка на рассылку новостей
- subscribe() - получить последние новости
- onSubscribe() - успешная подписка + детали отмены
- processor - объединяет все новости в 1 сообщение
- без почтового ящика -> 1 письмо в день
- без user auth
- scheduledPublisher - при подписке 1 раз в день отправлять свежие новости
- кешировать последнее письмо, evict 1 раз
- многоуровневая рассылка 1 поток для всех подписчиков


Характеристики производительности веб-приложений: 
1) пропускная способность (X =число пользователей в секунду), 
2) задержка (R=среднее время отклика), 
3) потребление памяти.

N = X × R. - среднее количество запросов (N) обрабатываемых одновременно (закон Литтла)

Чтобы система обрабатывала до X = 100 запросов в секунду, и среднее время отклика R системы составляло 0,2 секунд,
нужно параллельно обслуживать до 20 запросов (=пользователей)=> нужно 20 потоков выполнения или несколько машин.

- Нагрузочное тестирование с 20тыс пользователей одновременно

### Полезные материалы:

https://www.youtube.com/watch?v=y3ySZkSgWik Mongo + Reactive examples

https://github.com/mongodb-developer/mdb-spring-boot-reactive/blob/main/src/main/java/com/example/mdbspringbootreactive/config/ReactiveMongoConfig.java

https://www.youtube.com/watch?v=6zfIxgaVkQI Thymeleaf

https://spring.io/guides/gs/reactive-rest-service

https://github.com/schananas/practical-reactor/blob/main/exercises/pom.xml

https://github.com/PacktPublishing/Hands-On-Reactive-Programming-in-Spring-5/blob/master/chapter-03/news-service/src/main/java/org/rpis5/chapters/chapter_03/news_service/NewsServiceApp.java
