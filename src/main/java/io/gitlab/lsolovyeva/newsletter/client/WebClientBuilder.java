/*package io.gitlab.lsolovyeva.newsletter.client;

import io.gitlab.lsolovyeva.newsletter.model.News;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static org.springframework.http.HttpStatus.EXPECTATION_FAILED;

public class WebClientBuilder {

    public void createClientGet() {
        WebClient.create("http://localhost/api") // base URI
                .get() // HTTP method
                .uri("/users/{id}", userId)
                .retrieve() // retrieve=getting request body, vs exchange ?
                .bodyToMono(News.class) // transform News request to Mono response
                .map()
                .subscribe(); // create connection and send request to remote server
    }

    public void createClientPost() {
        WebClient.Builder webClientBuilder;

        WebClient webClient = webClientBuilder.baseUrl("http://localhost:8080").build();

        webClient
                .post()
                .uri("/check") //
                .body(BodyInserters.fromPublisher(
                        Mono.just(new PasswordDTO("raw", "encoded")),
                        PasswordDTO.class)) // create body
                .exchange()
                .flatMap(response -> { // processing response
                    if (response.statusCode().is2xxSuccessful()) {
                        return Mono.empty();
                    } else if (response.statusCode() == EXPECTATION_FAILED) {
                        return Mono.error(new BadCredentialsException()
                        );
                    }
                    return Mono.error(new IllegalStateException());
                });
    }
}
*/