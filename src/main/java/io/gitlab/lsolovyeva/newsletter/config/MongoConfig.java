package io.gitlab.lsolovyeva.newsletter.config;

import io.gitlab.lsolovyeva.newsletter.converter.LocalDateTimeReadConverter;
import io.gitlab.lsolovyeva.newsletter.converter.LocalDateTimeWriteConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class MongoConfig {
    @Bean
    public MongoCustomConversions mongoCustomConversions() {
        List<Converter<?, ?>> converters = new ArrayList<>();
        converters.add(new LocalDateTimeReadConverter());
        converters.add(new LocalDateTimeWriteConverter());
        return new MongoCustomConversions(converters);
    }
}
