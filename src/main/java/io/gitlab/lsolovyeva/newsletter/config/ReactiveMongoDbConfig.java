/*package io.gitlab.lsolovyeva.newsletter.config;

import com.mongodb.lang.NonNull;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@Configuration
@EnableReactiveMongoRepositories(basePackages = {"io.gitlab.lsolovyeva.newsletter.repository"})
public class ReactiveMongoDbConfig extends AbstractReactiveMongoConfiguration {


    @NonNull
    @Override
    protected String getDatabaseName() {
        return "news";
    }

    @Bean
    public MongoClient mongoClient() {
        return MongoClients.create();
    }

    @Bean
    public ReactiveMongoTemplate reactiveMongoTemplate() {
        return new ReactiveMongoTemplate(mongoClient(), getDatabaseName());
    }

    /*@Override
    protected void configureClientSettings(MongoClientSettings.Builder builder) {
        builder
                .credential(MongoCredential.createCredential("user", "news", "password".toCharArray()))
                .applyToClusterSettings(settings  -> {
                    settings.hosts(Collections.singletonList(new ServerAddress("localhost", Integer.parseInt("27017"))));

                });
       /* builder.applyConnectionString(new ConnectionString(mongoProperties.getUri()))
                .readConcern(ReadConcern.SNAPSHOT)
                .writeConcern(WriteConcern.MAJORITY);
    }

    /*@Bean
    ReactiveMongoTransactionManager transactionManager(ReactiveMongoDatabaseFactory dbFactory) {
        return new ReactiveMongoTransactionManager(dbFactory);
    }*/

     /*@Bean
     public ReactiveMongoClientFactoryBean mongo() {
        ReactiveMongoClientFactoryBean mongo = new ReactiveMongoClientFactoryBean();
        mongo.setHost("localhost");
        return mongo;
    }*/

    /*@Bean
    MongoClient mongoClient(MongoProperties properties) {
        ConnectionString connectionString = new ConnectionString(properties.determineUri());
        MongoClientSettings.Builder builder = MongoClientSettings
                .builder()
                .streamFactoryFactory(NettyStreamFactory::new)
                .applyToClusterSettings(b -> b.applyConnectionString(connectionString))
                .applyToConnectionPoolSettings(b -> b.applyConnectionString(connectionString))
                .applyToServerSettings(b -> b.applyConnectionString(connectionString))
                .applyToSslSettings(b -> b.applyConnectionString(connectionString))
                .applyToSocketSettings(b -> b.applyConnectionString(connectionString))
                .codecRegistry(fromRegistries(
                        MongoClients.getDefaultCodecRegistry(),
                        fromProviders(PojoCodecProvider.builder()
                                .automatic(true)
                                .register(News.class)
                                .build())
                ));

        if (connectionString.getReadPreference() != null) {
            builder.readPreference(connectionString.getReadPreference());
        }
        if (connectionString.getReadConcern() != null) {
            builder.readConcern(connectionString.getReadConcern());
        }
        if (connectionString.getWriteConcern() != null) {
            builder.writeConcern(connectionString.getWriteConcern());
        }
        if (connectionString.getApplicationName() != null) {
            builder.applicationName(connectionString.getApplicationName());
        }
        return MongoClients.create(builder.build());
    }
}
*/