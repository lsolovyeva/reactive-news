package io.gitlab.lsolovyeva.newsletter.mapper;

import io.gitlab.lsolovyeva.newsletter.entity.News;
import io.gitlab.lsolovyeva.newsletter.model.NewsRequest;
import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class NewsMapper {
    public static List<News> map(List<NewsRequest> newsRequestList) {
        List<News> news = new ArrayList<>();
        newsRequestList.forEach(r -> news.add(
                News.builder().owner(r.owner()).title(r.title()).text(r.text()).publishedDateTime(r.publishedDateTime())
                        .build()));
        return news;
    }
}
