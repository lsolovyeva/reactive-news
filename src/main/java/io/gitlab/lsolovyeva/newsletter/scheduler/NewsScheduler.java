package io.gitlab.lsolovyeva.newsletter.scheduler;

import io.gitlab.lsolovyeva.newsletter.service.NewsProcessorService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NewsScheduler {

    private final NewsProcessorService newsProcessorService;

    /**
     * Evict cache every day at 1:00 AM
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void syncRestaurantOrders() {
        newsProcessorService.cacheEvict();
    }
}
