package io.gitlab.lsolovyeva.newsletter.service;

import io.gitlab.lsolovyeva.newsletter.entity.News;
import io.gitlab.lsolovyeva.newsletter.repository.NewsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class NewsProcessorService {
    private final NewsRepository newsRepository;

    @Cacheable("latestNews")
    public Flux<News> findAllByPublishedDateTimeAfter(LocalDateTime dateTime) {
        return newsRepository.findAllByPublishedDateTimeAfter(dateTime);
    }

    public Mono<News> save(News news) {
        return newsRepository.save(news);
    }

    public Flux<News> saveAll(List<News> news) {
        return newsRepository.saveAll(news);
    }

    @CacheEvict(allEntries = true, value = "latestNews")
    public void cacheEvict() {
        log.info("Run cache evict.");
    }

    private Mono<News> updatedPublishedYearByTitle(Mono<String> title, Mono<Integer> newPublishingYear) {
        return Mono.zip(title, newPublishingYear).flatMap((Tuple2<String, Integer> data) -> {
            String titleVal = data.getT1();
            Integer yearVal = data.getT2();
            return newsRepository.findOneByTitle(titleVal).flatMap(news -> {
                LocalDateTime currentPublishedDateTime = news.getPublishedDateTime();
                LocalDateTime newPublishedDateTime = LocalDateTime.of(LocalDate.of(yearVal,
                                currentPublishedDateTime.getMonth(), currentPublishedDateTime.getDayOfMonth()),
                        currentPublishedDateTime.toLocalTime());
                news.setPublishedDateTime(newPublishedDateTime);
                return newsRepository.save(news);
            });
        });
    }

    /**
     * Метод, который выводит список новостей в виде одного сообщения с указанным префиксом message.
     *
     * @param message
     * @param news
     */
    private void reportResults(String message, Flux<News> news) {
        news.map(News::toString)
                .reduce(new StringBuilder(),
                        (sb, b) -> sb.append(" - ").append(b).append("\n"))
                .doOnNext(sb -> log.info(message + "\n{}", sb)).subscribe();
    }
}
