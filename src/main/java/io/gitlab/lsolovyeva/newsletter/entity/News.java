package io.gitlab.lsolovyeva.newsletter.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "NewsCollection")
public class News {

    @Id
    @Indexed(unique=true)
    private String id; //org.bson.types.ObjectId

    private String owner;

    private String title;

    private String text;

    private LocalDateTime publishedDateTime;
}
