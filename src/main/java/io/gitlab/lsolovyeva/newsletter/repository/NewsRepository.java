package io.gitlab.lsolovyeva.newsletter.repository;

import io.gitlab.lsolovyeva.newsletter.entity.News;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@Repository
public interface NewsRepository extends ReactiveMongoRepository<News, String> {
    Flux<News> findAllByPublishedDateTimeAfter(LocalDateTime startTime);

    Mono<News> findOneByTitle(String title);

}
