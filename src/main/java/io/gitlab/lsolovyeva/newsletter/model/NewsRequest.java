package io.gitlab.lsolovyeva.newsletter.model;

import java.time.LocalDateTime;

public record NewsRequest(String owner, String title, String text, LocalDateTime publishedDateTime) {

}
