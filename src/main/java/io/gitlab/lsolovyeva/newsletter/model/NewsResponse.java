package io.gitlab.lsolovyeva.newsletter.model;

import lombok.*;

@Data
@Builder
public class NewsResponse {

    private String id;

    private String owner;

    private String title;

    private String text;

    private String publishedOn;

}
