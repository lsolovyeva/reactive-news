package io.gitlab.lsolovyeva.newsletter.controller;

import io.gitlab.lsolovyeva.newsletter.model.NewsResponse;
import io.gitlab.lsolovyeva.newsletter.repository.NewsRepository;
import io.gitlab.lsolovyeva.newsletter.service.NewsProcessorService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static java.time.LocalDateTime.now;

@Controller
@AllArgsConstructor
public class NewsController {

    final NewsRepository newsRepository;
    final NewsProcessorService newsProcessorService;

    @GetMapping("/news")
    public Mono<Rendering> getLatestNewsWithoutDb() {
        List<NewsResponse> newsList = new ArrayList<>();
        LocalDateTime dateTime = now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm");

        String date = dateTime.format(formatter);
        newsList.add(NewsResponse.builder().id("1").owner("ls").title("Title 1").text("News 1").publishedOn(date).build());
        newsList.add(NewsResponse.builder().id("2").owner("ls").title("Title 2").text("News 2").publishedOn(date).build());

        return Mono.just(Rendering.view("lastNews")
                .modelAttribute("news", newsList)
                .build());
    }

    @GetMapping("/latest")
    public Mono<Rendering> getLatestNews() {
        Flux<NewsResponse> latestNews = newsProcessorService.findAllByPublishedDateTimeAfter(now().minusDays(7))
                .map(n -> {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm");
                    String publishedDate = n.getPublishedDateTime().format(formatter);
                    return NewsResponse.builder()
                            .id(n.getId())
                            .owner(n.getOwner())
                            .title(n.getTitle())
                            .text(n.getText())
                            .publishedOn(publishedDate)
                            .build();
                });
        return Mono.just(Rendering.view("lastNews")
                .modelAttribute("news", latestNews)
                .build());
    }

    /*@GetMapping("/news")
    public String getLatestNews() {// Mono<Rendering>
        Mono<News> accountMono = newsRepository.save(News.builder().id(1L).owner("ls").title("Title 1").text("News 1").build());
        Mono<News> accountMono2 = newsRepository.findById("1");
        /*Flux<News> newsList = newsRepository.findAll();
        return Mono.just(Rendering.view("lastNews")
                .modelAttribute("news", newsList)
                .build());
        return "ok";
    }*/

    /*@RequestMapping("/play-list-view")
    public String view(final Model model) {
        final Flux<News> newsStream;
        model.addAttribute(
                "playList",
                new ReactiveDataDriverContextVariable(newsStream, 1, 1));
        return "thymeleaf/news-list-view";
    }

    public Mono<ServerResponse> create(ServerRequest request) {
        return request
                .bodyToMono(News.class)
                .flatMap(newsRepository::save)
                .flatMap(o -> ServerResponse.created(URI.create("/news/" + o.getId())).build());
    }*/
}
