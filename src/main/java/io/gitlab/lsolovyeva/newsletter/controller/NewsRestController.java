package io.gitlab.lsolovyeva.newsletter.controller;

import io.gitlab.lsolovyeva.newsletter.entity.News;
import io.gitlab.lsolovyeva.newsletter.model.NewsRequest;
import io.gitlab.lsolovyeva.newsletter.service.NewsProcessorService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static io.gitlab.lsolovyeva.newsletter.mapper.NewsMapper.map;
import static java.time.LocalDateTime.now;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/news")
public class NewsRestController {

    final NewsProcessorService newsProcessorService;

    @PostMapping("/upload")
    public Mono<News> uploadLatestNews(@RequestBody NewsRequest newsRequest) {
        Mono<News> news = newsProcessorService.save(News.builder().owner(newsRequest.owner())
                .title(newsRequest.title()).text(newsRequest.text()).publishedDateTime(newsRequest.publishedDateTime())
                .build());
        return news;
    }

    @PostMapping("/upload")
    public void uploadAllLatestNews(@RequestBody List<NewsRequest> newsRequestList) {
        newsProcessorService.saveAll(map(newsRequestList)).then()
                .doOnSuccess(ignore -> log.info("News saved in DB."))
                .subscribe(data -> log.info("onNext: {}", data), err -> {}, () -> log.info("onComplete"));
    }

    @GetMapping("/latest")
    public Flux<News> getLatestNews() {
        Flux<News> latestNews = newsProcessorService.findAllByPublishedDateTimeAfter(now().minusDays(7));
        return latestNews;
    }
}
