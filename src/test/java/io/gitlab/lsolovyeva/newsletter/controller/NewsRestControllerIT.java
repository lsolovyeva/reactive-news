package io.gitlab.lsolovyeva.newsletter.controller;

import io.gitlab.lsolovyeva.newsletter.entity.News;
import io.gitlab.lsolovyeva.newsletter.repository.NewsRepository;
import io.gitlab.lsolovyeva.newsletter.service.NewsProcessorService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static java.time.LocalDateTime.now;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ImportAutoConfiguration(TestWebClientBuilderConfiguration.class)
@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = {NewsRestController.class})
@AutoConfigureWebTestClient
class NewsRestControllerIT {

    @Autowired
    WebTestClient client;

    @MockBean
    ExchangeFunction exchangeFunction;

    @MockBean
    ReactiveMongoTemplate reactiveMongoTemplate;

    @MockBean
    NewsRepository newsRepository;

    @MockBean
    NewsProcessorService newsProcessorService;

    @Test
    public void testUploadLatestNews() {
        News testNews = News.builder().owner("ls").title("Title Old").text("News Old").publishedDateTime(now().minusDays(10)).build();
        when(newsProcessorService.save(any())).thenReturn(Mono.just(testNews));
        client.post()
                .uri("/api/v1/news/upload")
                .bodyValue(new News())
                .exchange()
                .expectStatus().is2xxSuccessful()
                .returnResult(String.class).getResponseBody().as(StepVerifier::create)
                .expectNextCount(1)
                .expectComplete()
                .verify();
        verify(newsProcessorService, times(1)).save(any());
    }

    @Test
    public void testGetLatestNews() {
        Flux<News> testNews = Flux.just(new News(), new News());
        when(newsProcessorService.findAllByPublishedDateTimeAfter(any())).thenReturn(testNews);
        //when(exchangeFunction.exchange(any(ClientRequest.class))).thenReturn(Mono.just(ClientResponse.create(HttpStatusCode.valueOf(201)).build()));
        client.get()
                .uri("/api/v1/news/latest")
                .exchange().expectStatus().is2xxSuccessful().returnResult(News.class).getResponseBody().as(StepVerifier::create).expectNext(new News(), new News()).expectComplete()
                .verify();
        //verify(exchangeFunction).exchange(any());
    }

    @Test
    public void test() {
        WebTestClient webTestClient = WebTestClient.bindToServer()
                .baseUrl("https://lsolovyeva.gitlab.io/aerial-gymnastics-encyclopedia/")
                .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024)).build();
        webTestClient.get().exchange().expectStatus().is2xxSuccessful().expectHeader().exists("Etag").expectBody().xpath("lsolovyeva");
    }
}
