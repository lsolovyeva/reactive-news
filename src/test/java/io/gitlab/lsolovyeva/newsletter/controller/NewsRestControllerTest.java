package io.gitlab.lsolovyeva.newsletter.controller;

import io.gitlab.lsolovyeva.newsletter.entity.News;
import io.gitlab.lsolovyeva.newsletter.service.NewsProcessorService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@ExtendWith(MockitoExtension.class)
class NewsRestControllerTest {

    @Mock
    private NewsProcessorService newsProcessorService;

    @Test
    void getLatestNews() {
        Flux<News> testNews = Flux.just(News.builder().title("Book1").build());
        when(newsProcessorService.findAllByPublishedDateTimeAfter(any())).thenReturn(testNews);

        NewsRestController controller = new NewsRestController(newsProcessorService);
        WebTestClient
                .bindToController(controller).build()
                .get().uri("/api/v1/news/latest")
                .exchange().expectHeader().contentTypeCompatibleWith(APPLICATION_JSON).expectStatus().is2xxSuccessful().returnResult(News.class)
                .getResponseBody()
                .as(StepVerifier::create)
                .expectNextCount(1)
                .expectComplete()
                .verify();
    }
}
