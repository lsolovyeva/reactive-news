package io.gitlab.lsolovyeva.newsletter.service;

import io.gitlab.lsolovyeva.newsletter.entity.News;
import io.gitlab.lsolovyeva.newsletter.repository.NewsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static java.time.LocalDateTime.now;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NewsProcessorServiceTest {

    @Mock
    NewsRepository newsRepository;
    NewsProcessorService newsProcessorService;

    @BeforeEach
    void setup() {
        newsProcessorService = new NewsProcessorService(newsRepository);
        when(newsRepository.findAllByPublishedDateTimeAfter(any())).thenReturn(Flux.just(News.builder().title("Book1").build()));
    }
    @Test
    void findAllByPublishedDateTimeAfter() {
        Flux<News> result =  newsProcessorService.findAllByPublishedDateTimeAfter(now());
        StepVerifier.create(result)
                .expectNextMatches(r -> r.getTitle().equals("Book1"))
                .verifyComplete();
    }
}
