package examples;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import reactor.core.Disposable;
import reactor.core.publisher.BufferOverflowStrategy;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.List;

@Disabled("One-time checks")
public class ExamplesTest {

    private static final Flux<String> testFlux = Flux.fromIterable(List.of("f1", "f2", "f3")).log();

    @Test
    void publishMonoAndSubscribe() {
        Mono<String> testMono = Mono.just("testMonoPublisher").log();
        testMono.subscribe(System.out::println);
    }

    @Test
    void publishNullMonoAndSubscribe() {
        Mono<String> testMono = Mono.just(null);
        testMono.subscribe(System.out::println);
    }

    @Test
    void publishEmptyMonoAndSubscribe() {
        Mono<String> testMono = Mono.empty();
        testMono.subscribe(System.out::println);
    }

    @Test
    void publishFluxAndSubscribe() {
        Flux<String> testFlux1 = Flux.just("f01", "f02", "f03").log();
        testFlux1.subscribe(System.out::println);
        testFlux.subscribe(System.out::println);
    }

    @Test
    void fluxMapSync() {
        testFlux.map(String::toUpperCase).subscribe(System.out::println);
    }

    @Test
    void fluxFlatMapAsync() {
        testFlux.flatMap(s -> Mono.just(s.toUpperCase())).subscribe(System.out::println);
    }

    @Test
    void fluxSkipTwoElements() {
        testFlux.skip(2).subscribe(System.out::println);
    }

    @Test
    void fluxSkipAllElements() {
        testFlux.skip(5).subscribe(System.out::println);
    }

    @Test
    void fluxDelay() throws InterruptedException {
        testFlux.delayElements(Duration.ofSeconds(2)).skipLast(1).subscribe(System.out::println);
        Thread.sleep(10_000);
    }

    @Test
    void fluxConcat() throws InterruptedException {
        Flux.concat(testFlux.delayElements(Duration.ofSeconds(1)), testFlux.delayElements(Duration.ofSeconds(1))).subscribe(System.out::println);
        Thread.sleep(10_000);
    }

    @Test
    void fluxMerge() throws InterruptedException {
        Flux.merge(testFlux.delayElements(Duration.ofSeconds(1)), testFlux.delayElements(Duration.ofSeconds(1))).subscribe(System.out::println);
        Thread.sleep(10_000);
    }

    @Test
    void fluxZip() {
        Flux.zip(Flux.range(1, 5), Flux.range(6, 10), Flux.range(11, 15)).subscribe(System.out::println);
    }

    @Test
    void fluxCollect() {
        testFlux.collectList().subscribe(System.out::println);
    }

    @Test
    void fluxBuffer() {
        testFlux.buffer(2).subscribe(System.out::println);
    }

    @Test
    void fluxCollectMap() {
        testFlux.collectMap(s -> s.concat("*")).subscribe(System.out::println);
    }

    @Test
    void fluxDoOnEach() {
        testFlux.doOnEach(signal -> {
                    if (signal.getType() == SignalType.ON_COMPLETE) {
                        System.out.println("Completed!");
                    }
                }
        ).subscribe(System.out::println);

        testFlux.doOnComplete(() -> System.out.println("Completed!")).subscribe(System.out::println);
    }

    @Test
    void fluxDoOnCancel() throws InterruptedException {
        Disposable disposable = testFlux.delayElements(Duration.ofSeconds(1)).doOnCancel(() -> System.out.println("Cancelled!")).subscribe(System.out::println);
        Thread.sleep(3_000);
        disposable.dispose();
    }

    @Test
    void fluxErrorHandling() {
        testFlux.map(l -> {
                    if (l.equals("f2")) {
                        throw new RuntimeException("Unexpected element");
                    }
                    return l;
                }
        ).onErrorContinue((throwable, o) -> System.out.println("Don't worry about ".concat(o.toString()).concat("!"))).subscribe(System.out::println);

        testFlux.map(l -> {
                    if (l.equals("f2")) {
                        throw new RuntimeException("Unexpected element");
                    }
                    return l;
                }
        ).onErrorReturn("-1").subscribe(System.out::println);

        testFlux.map(l -> {
                    if (l.equals("f2")) {
                        throw new RuntimeException("Unexpected element");
                    }
                    return l;
                }
        ).onErrorMap(throwable -> new UnsupportedOperationException(throwable.getMessage())).subscribe(System.out::println);
    }

    @Test
    void fluxNoOverflow() {
        Flux.range(1, Integer.MAX_VALUE)
                .log()
                .concatMap(x -> Mono.delay((Duration.ofMillis(100))))
                .blockLast();
    }

    @Test
    void fluxOverflow() {
        Flux.interval(Duration.ofMillis(1))
                .log()
                .concatMap(x -> Mono.delay((Duration.ofMillis(100))))
                .blockLast();
    }

    @Test
    void fluxDropOnBackpressure() {
        Flux.interval(Duration.ofMillis(1))
                .onBackpressureDrop()
                .concatMap(x -> Mono.delay((Duration.ofMillis(100))).thenReturn(x))
                .doOnNext(x -> System.out.println("Element kep by consumer: ".concat(x.toString())))
                .blockLast();
    }

    @Test
    void fluxBufferOnBackpressure() {
        Flux.interval(Duration.ofMillis(1))
                .onBackpressureBuffer(50, BufferOverflowStrategy.DROP_OLDEST)
                .concatMap(x -> Mono.delay((Duration.ofMillis(100))).thenReturn(x))
                .doOnNext(x -> System.out.println("Element kep by consumer: ".concat(x.toString())))
                .blockLast();
    }

    @Test
    void reactiveTestWithStepVerifier() {
        StepVerifier.create(Flux.just("foo", "bar"))
                .expectSubscription()
                .expectNext("foo")
                .expectNext("bar")
                .expectComplete()
                .verify();
        StepVerifier.create(Flux.range(0, 100))
                .expectSubscription()
                .expectNext(0)
                .expectNextCount(98)
                .expectNext(99)
                .expectComplete()
                .verify();
        StepVerifier.create(Flux.just("alpha-foo", "betta-bar"))
                .expectSubscription()
                .expectNextMatches(e -> e.startsWith("alpha"))
                .expectNextMatches(e -> e.startsWith("betta"))
                .thenCancel()
                .verify();
    }
}
