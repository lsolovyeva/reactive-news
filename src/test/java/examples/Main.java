package examples;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        System.out.println(textModifier());
    }

    public static String textModifier() {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        int index = s.indexOf("  ");
        while (index != -1) {
            s = s.substring(0, index).concat(s.substring(index + 1));
            index = s.indexOf("  ");
        }

        char[] array = s.toCharArray();
        for (int i = 1; i < array.length; i++) {
            if (array[i] == '-') {
                char tmp = array[i - 1];
                array[i - 1] = array[i + 1];
                array[i + 1] = tmp;
            }
        }
        s = String.valueOf(array).replaceAll("-", "");

        s = s.replaceAll("\\+", "!");

        int sum = 0;

        Pattern digitRegex = Pattern.compile("\\d");
        Matcher matcher = digitRegex.matcher(s);

        while (matcher.find()) {
            sum += Integer.parseInt(matcher.group());
        }
        s = s.replaceAll("\\d", "");

        if (sum != 0) {
            s = s.concat(" " + sum);
        }

        return s;
    }
}
